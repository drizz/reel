## Interface: 70000
## Title: Reel
## Notes: A helping hand for fishing.
## Author: drizz
## Version: 0.3.1
## SavedVariables: Reel_Options

API.lua
Reel.lua
Lures.lua
Reel.xml

States\EquipFishingGear.lua
States\UseBuff.lua
States\UseLure.lua
States\Catching.lua
States\Looting.lua
States\Moving.lua
States\Fishing.lua
